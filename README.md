# Tezos protocol simulator

This software allows to simulate non-standard baking strategies.

Type `make init` and then `make` to build.

Usage: `./simu` should display a list of available commands.
The binary functions in two modes:
- data generation
For instance,
```
./simu sample -a iar-attacker -p emmy+C -s 879874 -n 300
```
will generate data for the adversary called 'two-step-profitable-selfish' on protocol 'emmy+C'
with seed 879874 and 300 random chains per value of the adversarial stake.
This will generate a file called `two-step-profitable-selfish-emmy+C-1.0-1.0-87987.trace`

- plotting indicators
```
./simu -- compute AR on ./two-step-profitable-selfish-emmy+C-1.0-1.0-87987.trace --show
```
will compute the AR indicator for the provided file(s). The display system will gather
all adversaries in a given protocol in the sample plot. The --show option triggers displaying
the result, in all cases a .png file is produced with the result.

- json output
```
./simu format rsd-attacker-emmy+B-dh=1.0-hh=1.0-u=0.2-12345.trace
```
will convert simulation results into JSON format and save as `rsd-attacker-emmy+B-dh=1.0-hh=1.0-u=0.2-12345.trace.json`