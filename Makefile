.ONESHELL:

build:
	eval $(opam env)
	dune build ./simu.exe
	cp _build/default/simu.exe ./simu

init:
	eval $(opam env)
	dune clean
	dune build @install
	dune install

iar:
	./simu sample -a iar-attacker -p emmy+B -s 12345 -n 1000
	./simu format iar-attacker-emmy+B-dh=1.0-hh=1.0-u=0.2-12345.trace
	rm iar-attacker-emmy+B-dh=1.0-hh=1.0-u=0.2-12345.trace
	mv iar-attacker-emmy+B-dh=1.0-hh=1.0-u=0.2-12345.trace.json ./results/iar-emmy+b.json

	./simu sample -a iar-attacker -p emmy+C -s 12345 -n 1000
	./simu format iar-attacker-emmy+C-dh=1.0-hh=1.0-u=0.2-12345.trace
	rm iar-attacker-emmy+C-dh=1.0-hh=1.0-u=0.2-12345.trace
	mv iar-attacker-emmy+C-dh=1.0-hh=1.0-u=0.2-12345.trace.json ./results/iar-emmy+c.json

rsd:
	./simu sample -a rsd-attacker -p emmy+B -s 12345 -n 1000
	./simu format rsd-attacker-emmy+B-dh=1.0-hh=1.0-u=0.2-12345.trace
	rm rsd-attacker-emmy+B-dh=1.0-hh=1.0-u=0.2-12345.trace
	mv rsd-attacker-emmy+B-dh=1.0-hh=1.0-u=0.2-12345.trace.json ./results/rsd-emmy+b.json

	./simu sample -a rsd-attacker -p emmy+C -s 12345 -n 1000
	./simu format rsd-attacker-emmy+C-dh=1.0-hh=1.0-u=0.2-12345.trace
	rm rsd-attacker-emmy+C-dh=1.0-hh=1.0-u=0.2-12345.trace
	mv rsd-attacker-emmy+C-dh=1.0-hh=1.0-u=0.2-12345.trace.json ./results/rsd-emmy+c.json
